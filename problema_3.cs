﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[] { 1, 2, 3 };
            int[] b = new int[] { 7, 3 };

            string output = (f(a, b)) ? "ok" : "ko";

            Console.WriteLine(output);
            Console.ReadLine();
        }

        private static bool f(int[] a, int[] b)
        {
            return (a[0] == b[0] || a[a.Length - 1] == b[b.Length - 1]) ? true : false;
        }
    }
}
