﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_4
{
    class Program
    {
        static void Main(string[] args)
        {
            f("example.txt");
            Console.ReadLine();
        }

        private static void f(string s)
        {
            try
            {
                StreamReader sr = new StreamReader(s);
                List<string> ls = new List<string>();

                while (!sr.EndOfStream)
                    ls.Add(sr.ReadLine());

                ls.Sort();

                foreach (string st in ls)
                    Console.WriteLine(st);
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
