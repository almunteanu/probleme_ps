﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] ascii = new int[256];
            int c;

            Console.Write("Give me some input: ");
            do
            {
                c = Console.Read();
                if (c != '\r')
                    ascii[c]++;
            }
            while (c != '\r');

            for (int i = 0; i < ascii.Length; i++)
            {
                if (ascii[i] > 0)
                    Console.WriteLine((char)i + " - " + ascii[i]);
            }

            Console.ReadKey();
        }
    }
}
