﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Give me an integer: ");
            int n = Convert.ToInt32(Console.ReadLine());

            int sum = 0;

            while (n > 0)
            {
                sum += n % 10;
                n /= 10;
            }

            Console.WriteLine("Sum of digits is: {0}", sum);
            Console.ReadKey();
        }
    }
}
